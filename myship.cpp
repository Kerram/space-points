#include "Includes/myship.hpp"

MyShip::MyShip(std::string path) : Object(path)
{
	hp = 15;
	dmg = 5;
	speed = 4;
	cooldown = 750;
}

void MyShip::left()
{
	move(-speed, 0);
}

void MyShip::right()
{
	move(speed, 0);
}

void MyShip::down()
{
	move(0, speed);
}

void MyShip::up()
{
	move(0, -speed);
}

bool MyShip::attack()
{
	if(clock.getElapsedTime().asMilliseconds() >= cooldown)
	  {
		  clock.restart();
		  return true;
	  }
	
	return false;
}

void MyShip::addHp(int d)
{
    hp += d;
	hp = std::min(hp, 30);
}