#include "Includes/rocketeer.hpp"

Rocketeer::Rocketeer(std::string path) : Ship(path)
{
    hp = 50;
	speed = 3;
	score = 5000;
	dmg = 100;
	cooldown = 2500;
}

bool Rocketeer::attack()
{
    if(clock.getElapsedTime().asMilliseconds() >= cooldown)
	  {
		  clock.restart();
		  return true;
	  }
	
	return false;
}