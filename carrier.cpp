#include "Includes/carrier.hpp"

Carrier::Carrier(std::string path) : Ship(path)
{
    score = 1000;
	hp = 50;
	dmg = 0;
	speed = 1;
	ships_limit = 8;
	ships = 0;
	cooldown = 1500;
}

bool Carrier::isReady()
{
	if(ships >= ships_limit)
	  return false;
	
	if(clock.getElapsedTime().asMilliseconds() >= cooldown)
	  return true;
	
	return false;
}

void Carrier::created()
{
    ships++;
	clock.restart();
}