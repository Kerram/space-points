#include "Includes/image.hpp"

extern sf::RenderWindow render_window;
extern int SCREEN_HEIGHT;
extern int SCREEN_WIDTH;

Image::Image()
{
	width = 1;
	height = 1;
	x = 0;
	y = 0;
}

Image::Image(std::string path)
{
    x = 0;
	y = 0;
	load(path);
}

void Image::load(std::string path)
{
	texture.loadFromFile(path.c_str());
	width = texture.getSize().x;
	height = texture.getSize().y;
	
	sprite.setTexture(texture);
}

void Image::draw()
{
	render_window.draw(sprite);
}

void Image::set(int nx, int ny)
{
	x = nx;
	y = ny;
	
	sprite.setPosition(sf::Vector2f(nx, ny));
}

void Image::move(int dx, int dy)
{
	//getting sprite position
	sf::Vector2f tmp = sprite.getPosition();
	int posx = tmp.x;
	int posy = tmp.y;
	
	//ensuring sprite doesnt leave the screen
	dx = std::max(-posx + 1, dx);
	dx = std::min(SCREEN_WIDTH - posx - width - 1, dx);
	
	dy = std::min(SCREEN_HEIGHT - posy - height - 1, dy);
	dy = std::max(-posy + 1, dy);
	
	x += dx;
	y += dy;
	
	sprite.move(dx, dy);
}

int Image::getWidth() const
{
	return width;
}

int Image::getHeight() const
{
	return height;
}

int Image::getX() const
{
	return x;
}

int Image::getY() const
{
	return y;
}

bool Image::collision(const Image &img) const
{
	return (x <= img.getX() + img.getWidth()) && 
	(x + width >= img.getX()) && 
	(y <= img.getY() + img.getHeight()) &&
	(y + height >= img.getY());
}

void Image::setScale(float x, float y)
{
    height = float(height) * y;
	width = float(width) * x;
	
	sprite.setScale(x, y);
}