#include "Includes/engine.hpp"

extern int SCREEN_WIDTH;
extern int SCREEN_HEIGHT;
extern sf::RenderWindow render_window;

Engine::Engine()
{
    //initializing random
    std::srand(std::time(0));
    
	limit = 6;
	top = middle = bottom = 0;
	wave_time = 5000;
	delta_life = 1000;
	last_life = 0;
	ddmg_points = 5000;
	index = 0;
	level = 0;
	meteor_rain_active = false;
	meteor_active = false;
	meteor_cooldown = 450;
	meteor_time = 30000;
	
	//hero
	hero.set(375, 500);
	
	font.loadFromFile("Graphics/Fonts/arial.ttf");
	
	//sounds
	//loading buffers
	death_buffer.loadFromFile("Audio/Sounds/death.ogg");
	carrier_buffer.loadFromFile("Audio/Sounds/carrier.ogg");
	shot_buffer.loadFromFile("Audio/Sounds/shot.ogg");
	rayman_buffer.loadFromFile("Audio/Sounds/rayman.ogg");
	single_buffer.loadFromFile("Audio/Sounds/single.ogg");
	kaboom_buffer.loadFromFile("Audio/Sounds/kaboom.ogg");
	
	//loading sounds
	death_sound.setBuffer(death_buffer);
	shot_sound.setBuffer(shot_buffer);
	carrier_sound.setBuffer(carrier_buffer);
	single_sound.setBuffer(single_buffer);
	kaboom_sound.setBuffer(kaboom_buffer);
	rayman_sound.setBuffer(rayman_buffer);
	
	single_sound.setVolume(75);
	shot_sound.setVolume(25);
	
	//music
	music.openFromFile("Audio/Music/music.ogg");
	music.setLoop(true);
	
	music.setVolume(50);
}

Engine::~Engine()
{
    //erasing ships
	while(!ships.empty())
	  {
		  delete ships.back();
		  ships.pop_back();
	  }
	
	//erasing shots
	while(!shots.empty())
	  {
		  delete shots.back();
		  shots.pop_back();
	  }
	
	//erasing rockets
	while(!rockets.empty())
	  {
		  delete rockets.back();
		  rockets.pop_back();
	  }
	
	//erasing messages
	while(!messages.empty())
	  {
		  delete messages.front();
		  messages.pop();
	  }
}

void Engine::update()
{
    heroEvents();
    create();
    createShots();
    move();
    collisions();
	clearing();
}

void Engine::clearing()
{
    //checking if we are dead
    if(!hero.isAlive())
      {
		  gameover();
	  }
	
	//erasing dead enemies
	for(int i = int(ships.size()) - 1; i >= 0; i--)
	  {
		  if(!ships[i]->isAlive())
		    {
			    back.addScore(ships[i]->getScore());
				
				//checking if this ship is a kamikaze
				Kamikaze* tmp = dynamic_cast<Kamikaze*> (ships[i]);
				
				//if not
				if(tmp == NULL)
			      freeLevel(ships[i]->getY());
				
			    delete ships[i];
				ships.erase(ships.begin() + i);
		    }
	  }
	
	//erasing ships out of the screen (down)
	for(int i = int(ships.size()) - 1; i >= 0; i--)
	  {
		  int x = ships[i]->getX();
		  int y = ships[i]->getY();
		  int height = ships[i]->getHeight();
		  int width = ships[i]->getWidth();
		  
		  if(y + height >= SCREEN_HEIGHT - 1)
		    {
				delete ships[i];
				ships.erase(ships.begin() + i);
			}
	  }
	
	//erasing meteors out of the screen (left)
	for(int i = int(ships.size()) - 1; i >= 0; i--)
	  {
		  //if this 'ship' is not a meteor
		  if(!ships[i]->getImmortality())
			continue;
		  
		  int x = ships[i]->getX();
		  int y = ships[i]->getY();
		  int height = ships[i]->getHeight();
		  int width = ships[i]->getWidth();
		  
		  if(x <= 1)
		    {
				delete ships[i];
				ships.erase(ships.begin() + i);
			}
	  }
	
	//erasing used shots
	for(int i = int(shots.size()) - 1; i >= 0; i--)
	  {
		  if(!shots[i]->isAlive())
		    {
			    delete shots[i];
				shots.erase(shots.begin() + i);
		    }
	  }
	
	//erasing used rockets
	for(int i = int(rockets.size()) - 1; i >= 0; i--)
	  {
		  if(!rockets[i]->isAlive())
		    {
			    delete rockets[i];
				rockets.erase(rockets.begin() + i);
		    }
	  }
	
	//erasing shots out of the screen
	for(int i = int(shots.size()) - 1; i >= 0; i--)
	  {
		  int x = shots[i]->getX();
		  int y = shots[i]->getY();
		  int height = shots[i]->getHeight();
		  int width = shots[i]->getWidth();
		  
		  if((x <= 1) || (x + width >= SCREEN_WIDTH - 1) || (y <= 1) || (y + height >= SCREEN_HEIGHT - 1))
		    {
				delete shots[i];
				shots.erase(shots.begin() + i);
			}
	  }
	
	//erasing rockets out of the screen
	for(int i = int(rockets.size()) - 1; i >= 0; i--)
	  {
		  int x = rockets[i]->getX();
		  int y = rockets[i]->getY();
		  int height = rockets[i]->getHeight();
		  int width = rockets[i]->getWidth();
		  
		  if((x <= 1) || (x + width >= SCREEN_WIDTH - 1) || (y <= 1) || (y + height >= SCREEN_HEIGHT - 1))
		    {
				delete rockets[i];
				rockets.erase(rockets.begin() + i);
			}
	  }
	
	//erasing message
	if(!messages.empty())
	  {
		  if(!messages.front()->isAlive())
		    {
			    delete messages.front();
			    messages.pop();
				
				if(!messages.empty())
				  {
					  messages.front()->show();
				  }
		    }
	  }
}

void Engine::createMessage(std::string content)
{
    Message* tmp = new Message(font, content);
	
	if(messages.empty())
	  tmp->show();
	
	messages.push(tmp);
}

void Engine::setSettings(const Settings &s)
{
	sett = s;
	
	if(sett.game_mode)
	  back.setCampaign(true);
	
	if(sett.background)
	  back.load("Graphics/space2.png");
	
	if(sett.music)
	  music.play();
}

void Engine::createShots()
{
	for(int i = 0; i < int(ships.size()); i++)
	  {
		  if(ships[i]->attack())
		    createEnemyShot(ships[i]);
	  }
}

void Engine::createEnemyShot(Ship* ship)
{
    //if we are to create a rocket
    if(Rocketeer* rock = dynamic_cast<Rocketeer*> (ship))
	  {
		  Rocket* temp = new Rocket;
		  temp->set(ship->getX() + (ship->getWidth() / 2) - (temp->getWidth() / 2), ship->getY() + ship->getHeight() + 1);
		  temp->setDmg(ship->getDmg());
		  
		  rockets.push_back(temp);
		  
		  return;
	  }
    
	Shot* tmp = new Shot;
	
	if(ship->getDmg() == 10)
	  tmp->load("Graphics/shot2.png");
	
	tmp->setEnemy(true);
	tmp->set(ship->getX() + (ship->getWidth() / 2) - (tmp->getWidth() / 2), ship->getY() + ship->getHeight() + 1);
	tmp->setDmg(ship->getDmg());
	
	shots.push_back(tmp);
}

void Engine::events(const sf::Event event)
{
    bool all = true;
    
	for(int i = 0; i < int(ships.size()); i++)
	  {
		  if(meteor_active)
		    {
			    //if not a meteor
			    if(!ships[i]->getImmortality())
				  {
					  all = false;
					  break;
				  }
			}
		  
		  else
		    {
			    all = false;
				break;
			}
	  }
	
    //if it is campaign mode and we have killed all the enemies
    if((sett.game_mode) && (all))
	  nextLevel();
	
	//skipping level
	if((event.type == sf::Event::KeyPressed) && (event.key.code == sf::Keyboard::F1))
	  {
		  while(!ships.empty())
		    {
			    //checking if this ship is a kamikaze
				Kamikaze* tmp = dynamic_cast<Kamikaze*> (ships.back());
				
				//if not
				if(tmp == NULL)
			      freeLevel(ships.back()->getY());
			    
			    delete ships.back();
				ships.pop_back();
		    }
		  
		  nextLevel();
	  }
}

bool Engine::allFree()
{
	return (top == 0) && (middle == 0) && (bottom == 0);
}

void Engine::create()
{
    //horizontal meteors
    if(meteor_active)
	  {
		  if(meteor_cooldown_clock.getElapsedTime().asMilliseconds() >= meteor_cooldown)
		    {
			    createMeteor();
				meteor_cooldown_clock.restart();
			}
	  }
    
    //meteor rain
    if(meteor_rain_active)
	  {
		  //if it is time to stop
		  if(meteor_clock.getElapsedTime().asMilliseconds() >= meteor_time)
			meteor_rain_active = false;
		  
		  //if it is time to create new meteor
		  else if(meteor_cooldown_clock.getElapsedTime().asMilliseconds() >= meteor_cooldown)
		    {
			    createMeteor(false);
			    meteor_cooldown_clock.restart();
		    }
	  }
    
    //carriers
    for(int i = 0; i < int(ships.size()); i++)
	  {
		  //checking if this ship is a carrier
		  if(Carrier* tmp = dynamic_cast<Carrier*> (ships[i]))
		    {
			    if(tmp->isReady())
				  {
					  if(isFreeLevel())
					    {
						    createSingle();
							tmp->created();
						}
				  }
		    }
	  }
    
	//endless mode
	if(sett.game_mode)
	  return;

	//get the time
	sf::Time tmp = ship_clock.getElapsedTime();
	
	//if there is no enemy ship
	if(allFree())
	  {
		  createEnemyShip();
	  }
	
	//if it is time
	if(tmp.asMilliseconds() >= wave_time)
	  {
		  ship_clock.restart();
		  
		  if(isFreeLevel())
		    createEnemyShip();
	  }
	
	//bonuses
	//extra life
	if(back.getScore() >= last_life + delta_life)
	  {
		  last_life += delta_life;
		  extraLife();
	  }
	
	if(back.getScore() >= ddmg_points)
	  {
		  if(hero.getDmg() == 5)
		    {
			    doubleDamage();
		    }
	  }
}

int Engine::getFreeLevel(bool kamikaze)
{
	if(top < limit)
	  {
		  if(!kamikaze)
		    top++;
		  
		  return 1;
	  }
	
	if(middle < limit)
	  {
		  if(!kamikaze)
		    middle++;
		  
		  return 56;
	  }
	
	if(bottom < limit)
	  {
		  if(!kamikaze)
		    bottom++;
		  
		  return 111;
	  }
}

bool Engine::isFreeLevel()
{
	return (top < limit) || (middle < limit) || (bottom < limit);
}

void Engine::createSingle(int x, int y)
{
    if((sett.sounds) && (!sett.game_mode))
      single_sound.play();
    
	if(x == -100)
	  x = rand() % SCREEN_WIDTH;
	
	if(y == -100)
	  y = getFreeLevel();
	
	Single* tmp = new Single;
	tmp->set(x, y);
	ships.push_back(tmp);
}

void Engine::createDouble(int x, int y)
{
    if((sett.sounds) && (!sett.game_mode))
      single_sound.play();
    
	if(x == -100)
	  x = rand() % SCREEN_WIDTH;
	
	if(y == -100)
	  y = getFreeLevel();
	
	Single* tmp = new Single("Graphics/double.png");
	tmp->set(x, y);
	tmp->setDmg(10);
	tmp->setHp(15);
	tmp->setScore(300);
	ships.push_back(tmp);
}

void Engine::createKamikaze(int x, int y)
{
    if((sett.sounds) && (!sett.game_mode))
      kaboom_sound.play();
    
    if(x == -100)
	  x = rand() % SCREEN_WIDTH;
	
	if(y == -100)
	  y = 1;
	
	Kamikaze* tmp = new Kamikaze;
	tmp->set(x, y);
	
	ships.push_back(tmp);
}

void Engine::createCarrier(int x, int y)
{
    if((sett.sounds) && (!sett.game_mode))
      carrier_sound.play();
    
    if(x == -100)
	  x = rand() % SCREEN_WIDTH;
	
	if(y == -100)
	  y = getFreeLevel();
	
	Carrier* tmp = new Carrier;
	tmp->set(x, y);
	
	ships.push_back(tmp);
}

void Engine::createRayman(int x, int y)
{
    if((sett.sounds) && (!sett.game_mode))
      rayman_sound.play();
    
    if(x == -100)
	  x = rand() % SCREEN_WIDTH;
	
	if(y == -100)
	  y = getFreeLevel();
	
	Rayman* tmp = new Rayman;
	tmp->set(x, y);
	
	ships.push_back(tmp);
}

void Engine::createRocketeer(int x, int y)
{
    if(x == -100)
	  x = rand() % SCREEN_WIDTH;
	
	if(y == -100)
	  y = getFreeLevel();
	
	Rocketeer* tmp = new Rocketeer;
	tmp->set(x, y);
	
	ships.push_back(tmp);
}

void Engine::createMeteor(bool horizontal, int x, int y)
{
    float scalex = (float((rand() % 80) + 21)) / 100.0;
	float scaley = (float((rand() % 80) + 21)) / 100.0;
    
    Ship* tmp = new Ship("Graphics/meteor.png");
	tmp->setScale(scalex, scaley);
	tmp->setDirection(0);
    
    if(x == -100)
	  {
		  if(horizontal)
			x = SCREEN_WIDTH - 1 - tmp->getWidth();
		  
		  else
			x = rand() % SCREEN_WIDTH;
	  }
	
	if(y == -100)
	  {
		  if(horizontal)
			y = (rand() % (SCREEN_HEIGHT - 300 - tmp->getHeight())) + 200;
		  
		  else
			y = 1;
	  }
	
	tmp->setSpeed(4);
	tmp->set(x, y);
	tmp->setImmortality(true);
	
	if(!horizontal)
	  tmp->setDown(true);
	
	else
	  {
		  tmp->setRangex(SCREEN_WIDTH);
		  tmp->setDirection(-1);
		  tmp->setSpeed(2);
	  }
	
	ships.push_back(tmp);
}

void Engine::heroEvents()
{
	//moving hero
	//up
	if((sf::Keyboard::isKeyPressed(sf::Keyboard::Up)) || (sf::Keyboard::isKeyPressed(sf::Keyboard::W)))
	  {
		  hero.up();
	  }
	
	//down
	if((sf::Keyboard::isKeyPressed(sf::Keyboard::Down)) || (sf::Keyboard::isKeyPressed(sf::Keyboard::S)))
	  {
		  hero.down();
	  }
	  
	//left
	if((sf::Keyboard::isKeyPressed(sf::Keyboard::Left)) || (sf::Keyboard::isKeyPressed(sf::Keyboard::A)))
	  {
		  hero.left();
	  }
	  
	//right
	if((sf::Keyboard::isKeyPressed(sf::Keyboard::Right)) || (sf::Keyboard::isKeyPressed(sf::Keyboard::D)))
	  {
		  hero.right();
	  }
	  
	//shot
	if(sf::Keyboard::isKeyPressed(sf::Keyboard::Space))
	  {
		  if(hero.attack())
		    {
				createMyShot();
			}
	  }
}

void Engine::createMyShot()
{
    if(sett.sounds)
      shot_sound.play();
    
    Shot* tmp = new Shot;
    
    if(hero.getDmg() == 10)
	  {
		  tmp->load("Graphics/shot2.png");
	  }
	
	tmp->setEnemy(false);
	tmp->set(hero.getX() + (hero.getWidth() / 2) - (tmp->getWidth() / 2), hero.getY() - tmp->getHeight() - 1);
	tmp->setDmg(hero.getDmg());
	
	shots.push_back(tmp);
}

void Engine::draw()
{
    back.setHp(hero.getHp());
	back.draw(font);
	hero.draw();
	
	for(int i = 0; i < int(ships.size()); i++)
	  {
		  ships[i]->draw();
	  }
	
	for(int i = 0; i < int(shots.size()); i++)
	  {
		  shots[i]->draw();
	  }
	
	for(int i = 0; i < int(rockets.size()); i++)
	  {
		  rockets[i]->draw();
	  }
	
	if(!messages.empty())
	  {
		  messages.front()->draw();
	  }
}

void Engine::move()
{
	for(int i = 0; i < int(ships.size()); i++)
	  {
		  ships[i]->move();
	  }
	
	for(int i = 0; i < int(shots.size()); i++)
	  {
		  shots[i]->move();
	  }
	
	for(int i = 0; i < int(rockets.size()); i++)
	  {
		  rockets[i]->move(hero.getX());
	  }
}

void Engine::collisions()
{
	//collisions beetwen hero and ships
	for(int i = 0; i < int(ships.size()); i++)
	  {
		  //collision = death
		  if(hero.collision(*ships[i]))
		    {
				hero.damage(100);
			}
	  }
	
	//collisions of shots
	for(int i = 0; i < int(shots.size()); i++)
	  {
		  //damaging hero
		  if(shots[i]->getEnemy())
		    {
			    if(hero.collision(*shots[i]))
				  {
					  hero.damage(shots[i]->getDmg());
					  shots[i]->damage(100);
				  }
				
				//collisions with meteors
				for(int j = 0; j < int(ships.size()); j++)
				  {
					  //if meteor
					  if(ships[j]->getImmortality())
					    {
						    if(ships[j]->collision(*shots[i]))
						      shots[i]->damage(100);
					    }
				  }
		    }
		  
		  //damaging enemy
		  else
		    {
			    for(int j = 0; j < int(ships.size()); j++)
				  {
				      if(ships[j]->collision(*shots[i]))
					    {
						    ships[j]->damage(shots[i]->getDmg());
							shots[i]->damage(100);
					    }
				  }
		    }
	  }
	
	//collisions of rockets
	for(int i = 0; i < int(rockets.size()); i++)
	  {
		  //damaging hero
		  if(hero.collision(*rockets[i]))
			{
				hero.damage(rockets[i]->getDmg());
				rockets[i]->damage(100);
			}
		  
		  //collisions with ships
		  for(int j = 0; j < int(ships.size()); j++)
			{
				if(ships[j]->collision(*rockets[i]))
				  {
					  ships[j]->setImmortality(false);
					  ships[j]->damage(rockets[i]->getDmg());
					  rockets[i]->damage(100);
				  }
			}
	  }
	
	//collisions beetwen hero and rays
	for(int i = 0; i < int(ships.size()); i++)
	  {
		  //checking if this ship is a rayman
		  if(Rayman* tmp = dynamic_cast<Rayman*> (ships[i]))
		    {
			    if(tmp->rayCollision(hero))
				  hero.damage(tmp->getDmg());
			}
	  }
}

void Engine::gameover()
{
    if(sett.music)
      music.stop();
	
	if(sett.sounds)
      death_sound.play();
    
    if(!sett.game_mode)
      updateHighscore();
	
	//temporary variables
	sf::Event event;
	sf::Text text;
	
	text.setFont(font);
	text.setString("GAME OVER");
	text.setCharacterSize(100);
	text.setColor(sf::Color::Red);
	text.setPosition(85, 225);
	text.setStyle(sf::Text::Bold);
	
	//clone of main loop
	while(render_window.isOpen())
	  {
		  //check for events
		  while(render_window.pollEvent(event))
		    {
				if(event.type == sf::Event::Closed)
				  {
					  render_window.close();
				  }
				
				//exit
				if(event.type == sf::Event::KeyPressed)
				  {
					  render_window.close();
				  }
			}
		  
		  //drawing
		  render_window.clear(sf::Color::Black);
		  render_window.draw(text);
		  render_window.display();
	  }
}

void Engine::freeLevel(int y)
{
    if(y == 1)
	  top--;
    
	else if(y == 56)
	  middle--;
    
	else
	  bottom--;
}

void Engine::updateHighscore()
{
    std::ifstream fin("Data/highscore.sp");
	
	int tmp;
	fin>>tmp;
	
	if(tmp < back.getScore())
	  {
		  std::ofstream fout("Data/highscore.sp");
		  fout<<back.getScore()<<std::endl;
		  
		  fout.close();
	  }
	
	fin.close();
}

void Engine::load()
{
    //loading highscore
    if(!sett.game_mode)
	  {
         std::ifstream fin("Data/highscore.sp");
		 int tmp;
		 
		 fin>>tmp;
		 back.setHighscore(tmp);
		 
		 fin.close();
	  }
	
	//loading levels of campaign
	else
	  {
		 std::ifstream fin("Data/campaign.sp");
		 
		 fin>>levels;
		 
		 fin.close();
	  }
}

void Engine::createEnemyShip()
{
    int tmp = rand() % 100;
	
	if((tmp >= 0) && (tmp < 40))
	  createSingle();
	
	else if((tmp >= 40) && (tmp < 65))
	  createDouble();
	
	else if((tmp >= 65) && (tmp < 84))
	  createKamikaze();
	
	else if((tmp >= 84) && (tmp < 90))
	  createCarrier();
	
	else if((tmp >= 90) && (tmp < 100))
	  createRayman();
}

void Engine::extraLife()
{
    hero.addHp(5);
    createMessage("Extra Life!");
}

void Engine::doubleDamage()
{
    hero.setDmg(10);
	createMessage("Double\nDamage!");
}

void Engine::parse(char ch)
{
    if(ch == 'S')
	  createSingle();
	
	else if(ch == 'D')
	  createDouble();
	
	else if(ch == 'K')
	  createKamikaze();
	
	else if(ch == 'R')
	  createRayman();
	
	else if(ch == 'C')
	  createCarrier();
	
	else if(ch == 'X')
	  meteorRain();
	
	else if(ch == 'A')
	  createRocketeer();
	
	else if(ch == 'M')
	  meteors();
	
	else if(ch == 'B')
	  boss();
}

void Engine::campaignBonuses()
{
    if(level == 3)
	  doubleDamage();
	
	if((level == 2) || (level == 5) || (level == 9) || (level == 12) || (level == 14))
	  extraLife();
}

void Engine::nextLevel()
{
    meteor_rain_active = false;
	meteor_active = false;
	level++;
	
	if(levels[index] != 'X')
	  createMessage("Level " + std::to_string(level));
	
	else
	  createMessage("Meteor Rain!");
	
	campaignBonuses();
	
	while(levels[index] != ';')
	  {
		  parse(levels[index]);
		  index++;
	  }
	
	index++;
}

void Engine::meteorRain()
{
    meteor_cooldown = 450;
    
    meteor_clock.restart();
	meteor_cooldown_clock.restart();
	meteor_rain_active = true;
	
	createMeteor(false);
}

void Engine::meteors()
{
    meteor_cooldown = 1500;
    
    meteor_active = true;
	meteor_cooldown_clock.restart();
}

void Engine::boss()
{
    //temporary variables
	sf::Event event;
	sf::Text text;
	
	text.setFont(font);
	text.setString("BOSS\n\nComing Soon!");
	text.setCharacterSize(100);
	text.setColor(sf::Color::Red);
	text.setPosition(85, 25);
	text.setStyle(sf::Text::Bold);
	
	//clone of main loop
	while(render_window.isOpen())
	  {
		  //check for events
		  while(render_window.pollEvent(event))
		    {
				if(event.type == sf::Event::Closed)
				  {
					  render_window.close();
				  }
				
				//exit
				if(event.type == sf::Event::KeyPressed)
				  {
					  render_window.close();
				  }
			}
		  
		  //drawing
		  render_window.clear(sf::Color::Black);
		  back.draw(font);
		  render_window.draw(text);
		  render_window.display();
	  }
}