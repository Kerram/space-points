#include "Includes/single.hpp"

Single::Single(std::string path) : Ship(path)
{
	hp = 5;
	speed = 3;
	score = 100;
	dmg = 5;
	cooldown = 1250;
}

bool Single::attack()
{
	if(clock.getElapsedTime().asMilliseconds() >= cooldown)
	  {
		  clock.restart();
		  return true;
	  }
	
	return false;
}
