#include "Includes/ship.hpp"

Ship::Ship(std::string path) : Object(path)
{
	score = 0;
	rangex = 50;
	deltax = rangex;
	down = false;
	direction = 1;
}

void Ship::move()
{
	//change of direction
	if(deltax <= 0)
	  {
		  deltax = rangex;
		  
		  if(std::rand() % 2 == 0)
		    direction = -1;
		  
		  else
		    direction = 1;
	  }
	
	//moving
	if(direction != 0)
	  deltax -= speed;
	
	if(down)
	  Object::move(speed * direction, speed);
	
	else
	  Object::move(speed * direction, 0);
}

bool Ship::attack()
{
	return false;
}

int Ship::getScore()
{
    return score;
}

void Ship::setScore(int a)
{
    score = a;
}

void Ship::setDown(bool what)
{
    down = what;
}

void Ship::setDirection(int a)
{
    if(direction != a)
	  deltax = rangex;
    
	if((direction < -1) || (direction > 1))
	  direction = 0;
	
    direction = a;
}

void Ship::setRangex(int a)
{
    rangex = a;
	deltax = rangex;
}