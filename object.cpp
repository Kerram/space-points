#include "Includes/object.hpp"

extern int SCREEN_HEIGHT;
extern int SCREEN_WIDTH;

Object::Object(std::string path)
{
	hp = 0;
	speed = 0;
	immortal = false;
	
	image.load(path);
}

void Object::load(std::string path)
{
    image.load(path);
}

void Object::draw()
{
	image.draw();
}

void Object::set(int m_x, int m_y)
{
	image.set(m_x, m_y);
}

void Object::move(int dx, int dy)
{
	image.move(dx, dy);
}

int Object::getX()
{
	return image.getX();
}

int Object::getY()
{
	return image.getY();
}

int Object::getWidth()
{
	return image.getWidth();
}

int Object::getHeight()
{
	return image.getHeight();
}

int Object::getHp()
{
	return hp;
}

bool Object::collision(const Object &obj)
{
	return image.collision(obj.image);
}

void Object::setDmg(int a)
{
    dmg = a;
}

int Object::getDmg()
{
    return dmg;
}

void Object::damage(int a)
{
    if(!immortal)
	  hp -= a;
}

bool Object::isAlive()
{
    if(immortal)
	  return true;
    
	return hp > 0;
}

void Object::setHp(int a)
{
    hp = a;
}

void Object::setScale(float x, float y)
{
    image.setScale(x, y);
}

void Object::setImmortality(bool what)
{
    immortal = what;
}

void Object::setSpeed(int a)
{
    speed = a;
}

bool Object::getImmortality()
{
    return immortal;
}