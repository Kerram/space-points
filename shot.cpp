#include "Includes/shot.hpp"

Shot::Shot(std::string path) : Object(path)
{
	immortal = false;
	speed = 5;
	dmg = 5;
	hp = 1;
	enemy = false;
}

void Shot::setEnemy(bool what)
{
	enemy = what;
}

void Shot::move()
{
	if(enemy)
	  Object::move(0, speed);
	
	else
	  Object::move(0, -speed);
}

bool Shot::getEnemy()
{
	return enemy;
}