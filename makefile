CXX = g++
CXXFLAGS = -std=c++0x

LIBS = -lsfml-window -lsfml-graphics -lsfml-system -lsfml-audio -lsfgui

DEPS = gui.hpp myship.hpp image.hpp object.hpp engine.hpp background.hpp ship.hpp single.hpp shot.hpp message.hpp kamikaze.hpp carrier.hpp rayman.hpp rocket.hpp rocketeer.hpp

OBJ = main.o gui.o myship.o image.o object.o engine.o background.o ship.o single.o shot.o message.o kamikaze.o carrier.o rayman.o rocket.o rocketeer.o 

SpacePoints : $(OBJ)
	$(CXX) -o $@ $^ $(CXXFLAGS) $(LIBS)

%.o: %.c $(DEPS)
	$(CXX) -c -o $@ $< $(CXXFLAGS)

.PHONY: clean

clean:
	rm -f *.o