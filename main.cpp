#include <SFML/Graphics.hpp>
#include "Includes/gui.hpp"
#include "Includes/settings.hpp"
#include "Includes/engine.hpp"

//global settings
int SCREEN_WIDTH = 800;
int SCREEN_HEIGHT = 600;

//creating global render window
sf::RenderWindow render_window(sf::VideoMode(SCREEN_WIDTH, SCREEN_HEIGHT), "Space Points",
sf::Style::Titlebar | sf::Style::Close);

//utility function
Settings runGUI()
{
	GUI gui;
	return gui.run();
}

int main()
{
	//controling framerate
	render_window.setVerticalSyncEnabled(false);
	render_window.setFramerateLimit(60);
	
	//running gui
	Settings sett = runGUI();
	
	//creating game engine
	Engine engine;
	engine.setSettings(sett);
	engine.load();
	
	//temporary event
	sf::Event event;
	
	//main loop
	while(render_window.isOpen())
	  {
		  //check for events
		  while(render_window.pollEvent(event))
		    {
				if(event.type == sf::Event::Closed)
				  {
					  render_window.close();
				  }
				
				if(event.type == sf::Event::KeyPressed)
				  {
					  //exit
					  if(event.key.code == sf::Keyboard::Escape)
					    {
							render_window.close();
						}
				  }
				
				engine.events(event); //engine events
			}
		  
		  engine.update(); //updating engine
		  
		  //drawing
		  render_window.clear(sf::Color::Black);
		  engine.draw();
		  render_window.display();
	  }
	
	return 0;
}
