#include "Includes/background.hpp"

extern int SCREEN_HEIGHT;
extern int SCREEN_WIDTH;
extern sf::RenderWindow render_window;

Background::Background()
{
	campaign = false;
	score = highscore = 0;
	
	load("Graphics/space.png");
	life.load("Graphics/life.png");
	
	left.load("Graphics/left.png");
	left.set(0, SCREEN_HEIGHT - left.getHeight());
	
	right.load("Graphics/right.png");
	right.set(SCREEN_WIDTH - right.getWidth(), SCREEN_HEIGHT - right.getHeight());
	
	coord[0][0] = SCREEN_WIDTH - 5 - life.getWidth();
	coord[0][1] = SCREEN_HEIGHT - 13 - (life.getHeight() * 3);
	coord[1][0] = coord[0][0];
	coord[1][1] = SCREEN_HEIGHT - 8 - (life.getHeight() * 2);
	coord[2][0] = coord[0][0];
	coord[2][1] = SCREEN_HEIGHT - 3 - life.getHeight();
	coord[3][0] = SCREEN_WIDTH - 15 - (life.getWidth() * 2);
	coord[3][1] = coord[1][1];
	coord[4][0] = coord[3][0];
	coord[4][1] = coord[2][1];
	coord[5][0] = SCREEN_WIDTH - 25 - (life.getWidth() * 3);
	coord[5][1] = coord[2][1];
}

void Background::setCampaign(bool what)
{
	campaign = what;
}

void Background::load(std::string path)
{
	back.load(path);
}

void Background::setHighscore(int a)
{
	highscore = a;
}

int Background::getScore()
{
	return score;
}

void Background::addScore(int a)
{
	score += a;
	highscore = std::max(score, highscore);
}

void Background::draw(const sf::Font &font)
{
	back.draw();
	left.draw();
	right.draw();
	
	//creating text
	sf::Text text;
	text.setFont(font);
	text.setCharacterSize(25);
	
	//score
	text.setString(std::string("Score: ") + std::to_string(score));
	text.setPosition(sf::Vector2f(10, SCREEN_HEIGHT - 30));
	render_window.draw(text);
	
	//highscore
	if(!campaign)
	  {
		  text.setString(std::string("Highscore: ") + std::to_string(highscore));
		  text.setPosition(sf::Vector2f(10, SCREEN_HEIGHT - 60));
		  render_window.draw(text);
	  }
	
	//lives
	text.setString("Lives: ");
	text.setPosition(sf::Vector2f(SCREEN_WIDTH - right.getWidth() + 60, SCREEN_HEIGHT - 30));
	render_window.draw(text);
	
	for(int i = 0; i < hp / 5; i++)
	  {
		  life.set(coord[i][0], coord[i][1]);
		  life.draw();
	  }
}

void Background::setHp(int a)
{
    hp = a;
}
