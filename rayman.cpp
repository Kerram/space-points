#include "Includes/rayman.hpp"

Rayman::Rayman(std::string path, std::string path2) : Ship(path), ray(path2)
{
	score = 500;
	hp = 25;
	speed = 2;
	dmg = 100;
	cooldown = 2250;
	duration = 1000;
	active = false;
}

bool Rayman::attack()
{
    if(active)
	  {
		  if(clock.getElapsedTime().asMilliseconds() >= duration)
		    {
			    active = false;
				clock.restart();
		    }
	  }
	
	else
	  {
		  if(clock.getElapsedTime().asMilliseconds() >= cooldown)
		    {
			    active = true;
				clock.restart();
			}
	  }
    
    return false;
}

bool Rayman::rayCollision(const Object &obj)
{
    if(!active)
	  return false;
    
    return ray.collision(obj);
}

void Rayman::move()
{
    Ship::move();
	
	ray.set(getX(), getY() + getHeight() + 1);
}

void Rayman::draw()
{
    Ship::draw();
	
	if(active)
	  ray.draw();
}