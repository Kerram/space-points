#pragma once
#include <string>
#include "object.hpp"

class Ship : public Object
{
	protected:
	int score; //score for kill
	int rangex; //maximum deltax
	int deltax; //how much before random turn
	bool down; //is this ship going down
	int direction; //-1 = left, 1 = right, 0 = not moving
	
	public:
	Ship(std::string path);
	virtual void move(); //automatic move
	virtual bool attack(); //attack function
	int getScore(); //returns score
	void setScore(int a); //sets score
	void setDown(bool what); //sets down
	void setDirection(int a); //sets direction
	void setRangex(int a); //sets rangex
};
