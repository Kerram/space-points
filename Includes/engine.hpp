#pragma once
#include <SFML/Audio.hpp>
#include <SFML/System.hpp>
#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>
#include <vector>
#include <string>
#include <fstream>
#include <queue>
#include "background.hpp"
#include "myship.hpp"
#include "settings.hpp"
#include "ship.hpp"
#include "single.hpp"
#include "shot.hpp"
#include "message.hpp"
#include "kamikaze.hpp"
#include "carrier.hpp"
#include "rayman.hpp"
#include "rocket.hpp"
#include "rocketeer.hpp"

class Engine
{
	private:
	Background back; //score, lives, background style
	MyShip hero; //hero
	Settings sett; //settings
	sf::Music music; //music
	sf::Font font; //font
	sf::Clock meteor_clock; //meteor rain clock (campaign mode)
	sf::Clock meteor_cooldown_clock; //meteor cooldown clock (campaign mode)
	sf::Clock ship_clock; //how much time passed since last new ship
	std::vector <Ship*> ships; //ships
	std::vector <Shot*> shots; //shots
	std::vector <Rocket*> rockets; //rockets
	std::queue <Message*> messages; //messages
	int wave_time; //how many milliseconds between arrival of new ships
	int top; //how many ships on top
	int middle; //how many ships in the middle
	int bottom; //how many ships on the bottom
	int limit; //limit of ships (top, middle, bottom)
	int delta_life; //number of points beetwen extra lives (endless mode)
	int last_life; //number of points during last extra life (endless mode)
	int ddmg_points; //number of points required to get double damage (endless mode)
	std::string levels; //levels of campaign (campaign mode)
	int index; //last parsed character in levels + 1 (campaign mode)
	int level; //number of level (campaign mode)
	int meteor_time; //duration of meteor rain (campaign mode)
	int meteor_cooldown; //cooldown for next meteor (campaign mode)
	bool meteor_rain_active; //tells if the meteor rain is active (campaign mode)
	bool meteor_active; //tells if the horizontal metaors are active (campaign mode)
	sf::SoundBuffer death_buffer; //death buffer
	sf::SoundBuffer shot_buffer; //shot buffer
	sf::SoundBuffer rayman_buffer; //rayman buffer
	sf::SoundBuffer carrier_buffer; //carrier buffer
	sf::SoundBuffer single_buffer; //single buffer
	sf::SoundBuffer kaboom_buffer; //kaboom buffer
	sf::Sound death_sound; //death sound
	sf::Sound shot_sound; //shot sound
	sf::Sound rayman_sound; //rayman sound (endless mode)
	sf::Sound carrier_sound; //carrier sound (endless mode)
	sf::Sound single_sound; //single sound (endless mode)
	sf::Sound kaboom_sound; //kaboom sound (endless mode)
	
	void heroEvents(); //handling hero events
	void create(); //creating new ships
	void createShots(); //creates enemy shots
	void move(); //moves ships and shots
	void collisions(); //handles collisions
	void clearing(); //erasing objects
	void createMyShot(); //creates new shot
	void createEnemyShot(Ship* ship); //creates new enemy shot
	bool isFreeLevel(); //checks if there is a space for new ship
	int getFreeLevel(bool kamikaze = false); //returns first free level (top, middle, bottom)
	bool allFree(); //returns if there is no enemy ship
	void createSingle(int x = -100, int y = -100); //creates single ship
	void createDouble(int x = -100, int y = -100); //creates double ship
	void createKamikaze(int x = -100, int y = -100); //creates kamikaze ship
	void createCarrier(int x = -100, int y = -100); //creates carrier ship
	void createRayman(int x = -100, int y = -100); //creates rayman ship
	void createRocketeer(int x = -100, int y = -100); //creates rocketeer ship (campaign mode)
	void gameover(); //it means we are dead
	void freeLevel(int a); //updating number of ship on top, middle, bottom
	void updateHighscore(); //updating highscore in the file
	void createMessage(std::string content); //creates message
	void createEnemyShip(); //creates random enemy ship
	void extraLife(); //extra life bonus
	void doubleDamage(); //double damage bonus
	void nextLevel(); //initializes next level of campaign (campaign mode)
	void parse(char ch); //parses character from levels
	void campaignBonuses(); //bonuses in campaign (campaign mode)
	void meteorRain(); //creates meteor rain (campaign mode)
	void createMeteor(bool horizontal = true, int x = -100, int y = -100); //creates meteor (campaign mode)
	void meteors(); //activates horizontal meteors (campaign mode)
	void boss(); //boss fight (campaign mode)
	
	public:
	Engine();
	~Engine();
	void load(); //loads data from files
	void events(const sf::Event event); //handling events
	void update(); //updates engine
	void draw(); //draw everything
	void setSettings(const Settings &s); //set setttings
};
