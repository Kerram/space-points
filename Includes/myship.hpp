#pragma once
#include <SFML/System.hpp>
#include <string>
#include "object.hpp"

class MyShip : public Object
{
	private:
	sf::Clock clock; //clock for shots
	int cooldown; //cooldown for shots in milliseconds
	
	public:
	MyShip(std::string path = "Graphics/hero.png");
	void up(); //going up
	void left(); //going left
	void right(); //going right
	void down(); //going down
	bool attack(); //returns if we are ready to shoot
	void addHp(int d); //add d health points to hp
};
