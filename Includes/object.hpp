#pragma once
#include "image.hpp"

class Object
{
	protected:
	bool immortal; //defines if object is immortal
	int hp; //hp
	int dmg; //dmg
	int speed; //speed
	Image image; //image
	
	public:
    Object(std::string path);
	void load(std::string path); //load new file to the image
	void set(int x, int y); //sets position of image
	virtual void draw(); //draws object
	void move(int dx, int dy); //moves image
	int getX(); //returns x
	int getY(); //returns y
	int getWidth(); //returns width
	int getHeight(); //returns height
	int getHp(); //returns hp
	void setHp(int a); //sets hp
	bool collision(const Object &obj); //returns true if two objects are colliding
	void setDmg(int a); //sets dmg
	int getDmg(); //returns dmg
	void damage(int a); //damages object
	bool isAlive(); //returns if the object is alive
	void setScale(float x, float y); //sets scale of image
	void setImmortality(bool what); //sets immortality
	void setSpeed(int x); //sets speed
	bool getImmortality(); //returns if the object is immortal
};
