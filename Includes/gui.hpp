#pragma once
#include <SFML/Graphics.hpp>
#include <SFGUI/SFGUI.hpp>
#include <SFGUI/Widgets.hpp>
#include "settings.hpp"

class GUI
{
	private:
	sfg::SFGUI m_sfgui; //sfgui object
	bool exitClicked = false;
	bool settingsClicked = false;
	bool playClicked = false;
	
	public:
	//buttons events
	void onButtonClick1();
	void onButtonClick2();
	void onButtonClick3();
	void onButtonClick4();
	
	Settings run();
};
