#pragma once
#include <string>
#include "object.hpp"

class Shot : public Object
{
	private:
	bool enemy; //is this an enemy's shot
	
	public:
	Shot(std::string path = "Graphics/shot.png");
	void setEnemy(bool what); //sets if it is an enemy's shot
	void move(); //moves shot
	bool getEnemy(); //returns if it is an enemy's shot
};
