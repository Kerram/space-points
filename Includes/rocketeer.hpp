#pragma once
#include <SFML/System.hpp>
#include <string>
#include "ship.hpp"

class Rocketeer : public Ship
{
    private:
	sf::Clock clock; //clock for rockets
	int cooldown; //cooldown for rockets in milliseconds
    
    public:
	Rocketeer(std::string path = "Graphics/rocketeer.png");
	virtual bool attack(); //returns if we are ready to shoot
};