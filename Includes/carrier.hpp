#pragma once
#include <SFML/System.hpp>
#include <string>
#include "ship.hpp"

class Carrier : public Ship
{
    private:
	int ships; //number of created ships
	int ships_limit; //limit of created ships
	sf::Clock clock; //clock
	int cooldown; //cooldown for creating new ships in milliseconds
    
    public:
    Carrier(std::string path = "Graphics/carrier.png");
	bool isReady(); //returns if it is time to create new ship
	void created(); //it means we created new ship
};