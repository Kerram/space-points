#pragma once
#include <SFML/Graphics.hpp>
#include <string>
#include <algorithm>

class Image
{
	private:
	sf::Texture texture; //texture
	sf::Sprite sprite; //sprite
	int width; //width
	int height; //height
	int x; //x
	int y; //y
	
    public:
    Image(); //creates new empty image
    Image(std::string path); //creates new image from image file
    void load(std::string); //loads new image file
    void draw(); //draws image to the render window
    void set(int nx, int ny); //sets sprite coordinates to nx ny
    void move(int dx, int dy); //moves sprite and ensures it doesnt leave the screen
    int getHeight() const; //returns height
    int getWidth() const; //returns width
    int getX() const; //returns x
    int getY() const; //returns y
    bool collision(const Image &img) const; //returns true if two images are colliding
    void setScale(float x, float y); //sets scale
};
