#pragma once
#include <string>
#include <SFML/System.hpp>
#include "ship.hpp"
#include "object.hpp"

class Rayman : public Ship
{
    private:
	sf::Clock clock; //clock
	int duration; //how long the ray is active in milliseconds
	int cooldown; //cooldown for 'shots' in milliseconds
	Object ray; //image of ray
	bool active; //is the ray active?
    
    public:
	Rayman(std::string path = "Graphics/rayman.png", std::string path2 = "Graphics/ray.png");
	virtual bool attack(); //returns if we are ready to shot
	bool rayCollision(const Object &obj); //returns if object collides with the ray
	virtual void move(); //automatic move
	virtual void draw(); //draws rayman and ray
};