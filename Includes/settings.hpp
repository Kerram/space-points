#pragma once

struct Settings
{
	bool game_mode; //false - endless, true - campaign
	bool sounds;
	bool music;
	bool background; //false - space, true - surface
	
	Settings()
	{
		game_mode = sounds = music = background = false;
	}
};
