#pragma once
#include <string>
#include "object.hpp"

class Rocket : public Object
{
    public:
	Rocket(std::string path = "Graphics/rocket.png");
	void move(int x); //chases x
};