#pragma once
#include <string>
#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>

class Message
{
    private:
	sf::Text text; //text
	sf::Clock clock; //time from the beggining of the message
	int life_time; //how long the message will 'live' (in milliseconds)
	bool active; //tells if the message is active
    
	public:
	Message(const sf::Font &font, std::string mes = "");
	void setString(std::string a); //sets text
	void draw(); //draws text
	void show(); //activates the message
	bool isAlive(); //returns if the life time has passed
};