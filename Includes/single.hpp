#pragma once
#include <string>
#include "ship.hpp"

class Single : public Ship
{
	private:
	sf::Clock clock; //clock for shots
	int cooldown; //cooldown for shots in milliseconds
	
	public:
	Single(std::string path = "Graphics/single.png");
	virtual bool attack(); //returns if we are ready to shoot
};
