#pragma once
#include <string>
#include <SFML/Graphics.hpp>
#include "image.hpp"

class Background
{
	private:
	Image back; //background style
	Image left; //score
	Image right; //lives
	Image life; //live = 5hp
	bool campaign; //game mode
	int score; //score
	int highscore; //highscore
	int hp; //hp of our hero
	int coord[6][2]; //coordinates of lives icons
	
	public:
	Background();
	void load(std::string path); //loads background style
	void draw(const sf::Font &font); //draws background
	void setCampaign(bool what); //sets game mode
	void setHighscore(int a); //sets highscore
	int getScore(); //returns score
	void addScore(int a); //adds a points to the score
	void setHp(int a); //sets hp
};
