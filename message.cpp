#include "Includes/message.hpp"

extern sf::RenderWindow render_window;

Message::Message(const sf::Font &font, std::string mes)
{
    setString(mes);
	active = false;
	life_time = 2000;
	
	text.setFont(font);
	text.setCharacterSize(45);
	text.setColor(sf::Color::Red);
	text.setPosition(300, 225);
}

void Message::setString(std::string a)
{
    text.setString(a);
}

void Message::show()
{
    active = true;
	clock.restart();
}

bool Message::isAlive()
{
    if(!active)
	  return true;
    
    return clock.getElapsedTime().asMilliseconds() < life_time;
}

void Message::draw()
{
    if(active)
      render_window.draw(text);
}