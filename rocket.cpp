#include "Includes/rocket.hpp"

Rocket::Rocket(std::string path) : Object(path)
{
    immortal = false;
	speed = 2;
	dmg = 100;
	hp = 99;
}

void Rocket::move(int x)
{
    if(getX() > x)
      Object::move(-speed, speed);
	
	else if(getX() < x)
	  Object::move(speed, speed);
	
	else
	  Object::move(0, speed);
}